import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        usuario:{
            nombre:'Juan',
            edad: null,
        }
    },
    mutations:{
        setUsuarioNombre(state,nombre){
            state.usuario.nombre=nombre
        },
        setUsuarioEdad(state,edad){
            state.usuario.edad=edad
        },
    },
    getters:{

    },
    actions:{

    }

  })