import vue from 'vue';
import app from "./app.vue";
import router from "./router/router.js"
import store from "./store"

new vue({
    router,
    store,
    el:'#app',
    render:h=>h(app),
});